import { controls } from '../../constants/controls';

export async function fight(firstFighter, secondFighter) {
  return new Promise((resolve) => {
    const secondFighterTotalHealth = secondFighter.health;

    document.addEventListener(
      'keydown',
      (event) => {
        const keyCode = event.code;

        if (keyCode === controls.PlayerOneAttack) {
          let damage = getDamage(firstFighter, secondFighter);
          if (damage > 0) {
            secondFighter.health = secondFighter.health - damage;
            let healthIndicatorBarWidth = (secondFighter.health * 100) / secondFighterTotalHealth;
            healthIndicatorBarWidth = Math.floor(healthIndicatorBarWidth);
            document
              .getElementById('right-fighter-indicator')
              .setAttribute('style', `width: ${healthIndicatorBarWidth}%`);

            if (secondFighter.health < 0) {
              healthIndicatorBarWidth = 0;
              return resolve('first fighter won');
            }
          }
        }
      },
      false
    );
    // resolve the promise with the winner when fight is over
    // Функция fight должна возвращать промис, который будет выполнен успешно, если кто-то из бойцов побеждает.
  });
}

// Игроки наносят удары друг другу с помощью клавиш A (первый боец) и J (второй боец). Бойцы могут блокировать удары с помощью клавиш D и L соответственно, в таком случае боец ​​уклоняется от удара. Также боец ​​не может нанести удар, если он выставил блок.

export function getDamage(attacker, defender) {
  let attackerHit = getHitPower(attacker);
  let defenderDefense = getBlockPower(defender);
  let damage = attackerHit > defenderDefense ? attackerHit - defenderDefense : 0;
  return damage;
}

export function getHitPower(fighter) {
  // return hit power
  let criticalHitChance = randomNumber(1, 2); // not rounded
  let hitPower = fighter.attack * criticalHitChance;
  return hitPower;
}

export function getBlockPower(fighter) {
  let dodgeChance = randomNumber(1, 2); // not rounded
  let blockPower = fighter.defense * dodgeChance;
  return blockPower;
}

function randomNumber(min, max) {
  return Math.random() * (max - min) + min;
}

// Бойцы также могут наносить критические удары, которые не могут быть заблокированы и вычисляются по формуле 2 * attack, где attack - характеристика бойца. Для того, чтобы бойцу нанести такой удар, нужно одновременно нажать 3 соответствующие клавиши, указанные в файле controls.js. Этот удар можно наносить не чаще, чем каждые 10 секунд.
