import { createElement } from '../helpers/domHelper';

export function createFighterPreview(fighter, position) {
  const positionClassName = position === 'right' ? 'fighter-preview___right' : 'fighter-preview___left';
  const fighterElement = createElement({
    tagName: 'div',
    className: `fighter-preview___root ${positionClassName}`,
  });

  let fighterPropertiesList = document.createElement('ul');

  for (let property in fighter) {
    if (property == 'source' || property == '_id') continue;
    let listItem = document.createElement('li');
    let fighterInfo = document.createTextNode(property + ' : ' + fighter[property]);
    listItem.appendChild(fighterInfo);
    fighterPropertiesList.appendChild(listItem);
  }
  // console.log(fighter);
  fighterElement.appendChild(fighterPropertiesList);
  // fighterElement.appendChild(createFighterImage(fighter));

  // Object.keys(fighter).forEach(function (key) {
  //   let info = document.createTextNode(key);
  //   fighterElement.appendChild(info);
  // });
  // { _id: '5',
  // name: 'Ken',
  // health: 45,
  // attack: 3,
  // defense: 4,
  // source:
  //  'https://i.pinimg.com/originals/46/4b/36/464b36a7aecd988e3c51e56a823dbedc.gif' }

  // todo: show fighter info (image, name, health, etc.)

  return fighterElement;
}

export function createFighterImage(fighter) {
  const { source, name } = fighter;
  const attributes = {
    src: source,
    title: name,
    alt: name,
  };
  const imgElement = createElement({
    tagName: 'img',
    className: 'fighter-preview___img',
    attributes,
  });

  return imgElement;
}
