import { createElement } from '../../helpers/domHelper';
import { showModal } from './modal';
import App from '../../app';

export function showWinnerModal(fighter) {
  const winnerModal = {
    title: `${fighter.name} has won!`,
    bodyElement: 'we will be glad to see you again',
    onClose() {
      location.reload(true);
    },
  };
  showModal(winnerModal);
}
